# Non Empty Zipper

This data structure is useful when you want to have an item in a collection singled out. Things like slideshows, tabs, radio buttons and dropdown menus, are all items that have a "current" item in a list of items. The a NonEmptyZipper supports this structure in a manner that is not error prone.